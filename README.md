PostgreSQL performance farm client
==================================

A client integrating several database benchmarks intended for regular testing
of PostgreSQL during development, and reporting the results back to a server.
You can think of this as another buildfarm, but running performance tests
instead of regression tests. Also, this is written in Python and not Perl.

Currently there are three benchmarks available:

* pgbench (TPC-B test, testing OLTP workloads)
* TPC-H (analytics)
* TPC-DS (analytics, considered TPC-H on steroids)

None of these tests is meant to be a perfect implementation as specified by TPC,
for example the TPC-H and TPC-DS don't do any data refreshes (future work). So
don't try to use the results for comparison with results from other databases.

The goal is to provide feedback to developers, particularly detect performance
regressions soon and identify the commit(s) that probably caused them.


Limitations
-----------

The current client has a number of limitations - firstly, it only works on
Linux (and perhaps other unix-like operating systems - Solaris, BSD, ...).
If you're interested in making it work on Windows, let me know.


pgbench
-------

Requires no extra setup, everything is handled by the code (including data
generation etc.).


TPC-H and TPC-DS
----------------

The repository only includes query templates modified for PostgreSQL. The data
and query generators are not included, and you need to download them from
[tpc.org](http://www.tpc.org/tpch/).

The benchmark does not generate data on the fly, but requires you to do that
before starting the tests. The data generation is quite slow (at least compared
to pgbench), so generating it every time from scratch would be a waste of time.

So download the tools and build them. In case of TPC-H you'll need to prepare
a makefile first:

    $ cd tpch_2_17_0/dbgen
    $ cp makefile.suite makefile
    $ vim makefile

Set the variables like this (the `DATABASE` value does not matter much)

    CC = gcc
    DATABASE = ORACLE
    MACHINE = LINUX
    WORKLOAD = TPCH

and build the sources:

    $ make

after which you can generate data like this (scale 1 means roughly 1GB):

    $ ./dbgen -s 1

Sadly the format uses an extra delimiter at the end of the line, which we need
to fix for example using `sed`:

    $ for i in *.tbl; do
        sed 's/|$//' $i > ${i/.tbl/.csv};
        rm $i;
    done;

For TPC-DS the build process is pretty much exactly the same, except you don't
need to create the makefile.

The CSV files need to be stored in a directory accessible by the benchmarks,
and the last directory needs to be the scale - in this case the directory might
be `/data/tpch/1` as we've generated data for scale 1. The path (without the
scale part) is then passed to the benchmark using "cache_path" parameter.


Statistics
----------

The client also collects various system-level statistics, useful when analyzing
the results and investigating performance regressions or differences between
systems. This includes:

* various data from /proc (cpuinfo, meminfo, ...)
* PostgreSQL statistics (bgwriter, databases, tables and indexes)
* sar statistics
