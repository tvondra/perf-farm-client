import json
import math
import os
import psycopg2
import psycopg2.extras
import shutil
import time
import re

from multiprocessing import cpu_count, Process, Queue
from psycopg2.extensions import QueryCanceledError
from random import shuffle
from tempfile import mkdtemp
from utils.logging import log
from utils.misc import available_ram, run_cmd


class TPCDSBench(object):
	'a simple TPC-DS like benchmark, measuring timings of data load and 99 queries (no data refreshes)'

	def __init__(self, bin_path, dbgen_path, cache_path, dbname, scales = [1], streams = [1]):
		'''
		bin_path   - path to PostgreSQL binaries (dropdb, createdb, psql commands)
		dbgen_path - path to the TPC-H dbgen/qgen binaries
		dbname     - name of the database to use
		scales     - list of scales to test (default: 1)
		streams    - list of stream counts to test (default: 1)
		'''

		self._bin = bin_path
		self._dbgen = dbgen_path
		self._cache = cache_path
		self._dbname = dbname
		self._scales = scales
		self._streams = streams
		self._results = {}


	def _init_database(self):
		'recreate the database and create basic schema (i.e. tables)'

		log("recreating '%s' database" % (self._dbname,))

		run_cmd(['dropdb', '--if-exists', self._dbname], env={'PATH' : self._bin})
		run_cmd(['createdb', self._dbname], env={'PATH' : self._bin})

		filename = os.path.join(os.path.dirname(__file__), 'tpcds/schema.sql')
		run_cmd(['psql', self._dbname, '-c', ('\i %s' % (filename,))], env={'PATH' : self._bin})


	def _load_data(self, scale):
		'load data into the database, using existing data files (generated in advance)'

		# TODO allow loading compressed data (COPY FROM PROGRAM)

		# make sure the data directory exists
		if not os.path.exists('%s/%d' % (self._cache, scale)):
			raise Exception('data for scale %d does not exist' % (scale,))

		# load timings for each data set
		timings = {}

		for t in ['call_center',  'catalog_page',  'catalog_returns',  'catalog_sales',
				  'customer_address',  'customer',  'customer_demographics',  'date_dim',
				  'household_demographics', 'income_band', 'inventory', 'item',
				  'promotion', 'reason', 'ship_mode', 'store', 'store_returns',
				  'store_sales', 'time_dim', 'warehouse', 'web_page', 'web_returns',
				  'web_sales', 'web_site']:

			log("loading data for table '%s'" % (t,))
			sql = "COPY %(name)s FROM '%(dir)s/%(scale)d/%(name)s.csv' WITH (FORMAT csv, DELIMITER '|')" % {'dir' : self._cache, 'name' : t, 'scale' : scale}

			r = run_cmd(['psql', self._dbname, '-c', sql], env={'PATH' : self._bin})

			timings[t] = r[2]

		return timings


	@staticmethod
	def _load_script(filename):
		'load a script (with name:sql format) with filename relative to this source'

		lines = {}

		# all scripts are located in this module's subdirectory
		for l in open(os.path.join(os.path.dirname(__file__), filename), 'r'):
			tmp = [t.strip().lower() for t in l.split(':')]
			lines[tmp[0]] = tmp[1]

		return lines


	def _create_indexes(self):
		'create custom indexes on the database, return dict with timings'

		indexes = TPCDSBench._load_script('tpcds/indexes.sql')

		timings = {}

		for i in indexes:

			log("creating index '%s'" % (i,))

			r = run_cmd(['psql', self._dbname, '-c', indexes[i]], env={'PATH' : self._bin})
			timings[i] = r[2]

		return timings


	def _create_primary_keys(self):
		'create primary keys on the database, return dict with timings'

		keys = TPCDSBench._load_script('tpcds/primary-keys.sql')

		timings = {}

		for k in keys:

			log("creating primary key '%s'" % (k,))
			r = run_cmd(['psql', self._dbname, '-c', keys[k]], env={'PATH' : self._bin})
			timings[k] = r[2]

		return timings


	def _create_foreign_keys(self):
		'create foreign keys on the database, return dict with timings'

		keys = TPCDSBench._load_script('tpcds/foreign-keys.sql')

		timings = {}

		for k in keys:

			log("creating foreign key '%s'" % (k,))
			r = run_cmd(['psql', self._dbname, '-c', keys[k]], env={'PATH' : self._bin})
			timings[k] = r[2]

		return timings


	def _vacuum(self):
		'perform VACUUM ANALYZE on the database'

		log("vacuum analyze")
		r = run_cmd(['psql', self._dbname, '-c', 'vacuum analyze'], env={'PATH' : self._bin})
		return r[2]


	def _run_streams(self, duration, streams):
		'run a benchmark with given number of concurrent streams'

		# final results (merged from all workers)
		results = [{'count' : 0, 'timeouts' : 0, 'failures' : 0, 'runs' : []} for i in range(99)]
		result_queue = Queue()	# used to receive results from worker threads

		templatedir = os.path.join(os.path.dirname(__file__), 'tpcds/templates')

		# create and start a list of workers
		workers = [Process(target=tpcds_worker, args=(i+1, self._dbname, result_queue, QueryGenerator(self._dbgen, templatedir), duration)) for i in range(streams)]
		[w.start() for w in workers]

		# wait for all workers to complete
		for w in workers:
			w.join()

		# merge results from workers (simply fetch them from the queue until empty)
		while not result_queue.empty():

			r = result_queue.get()

			for i in range(99):
				results[i]['count'] += r[i]['count']
				results[i]['timeouts'] += r[i]['timeouts']
				results[i]['failures'] += r[i]['failures']
				results[i]['runs'].extend(r[i]['runs'])

		return results


	def run_tests(self, duration=60):
		'runs the TPC-DS benchmark'

		results = {}

		for scale in self._scales:

			result = {}

			log('initializing database for scale %d' % (scale,))

			self._init_database()

			result['load'] = self._load_data(scale)
			result['indexes'] = self._create_indexes()
			result['primary-keys'] = self._create_primary_keys()
			result['foreign-keys'] = self._create_foreign_keys()
			result['vacuum'] = self._vacuum()

			for streams in self._streams:
				log('running benchmark with %d streams' % (streams,))
				result[str(streams)] = self._run_streams(duration, streams)

			# store results of the runs
			results[str(scale)] = result

		return results


class QueryGenerator(object):
	'''class for generating random sequence of the 99 TPC-DS queries
	
	One important aspect is that it does not simply generate random sequence of
	numbers between 1 and 99, but sequence of shuffled batches of 99 queries
	(with all queries in random order). The goal is to run all queries.
	'''

	def __init__(self, qgen_path, template_path):
		'''
		qgen_path - path to the TPC-H dbgen/qgen binaries
		template_path - path to TPC-H templates (modified for PostgreSQL)
		'''

		self._qgen = qgen_path
		self._templates = template_path
		self._queries = range(1,100)
		shuffle(self._queries)
		self._current = 0
		self._tmpdir = mkdtemp()


	def _next_id(self):
		'get next random query ID, either from the current batch or a new one'

		# if we're out of query IDs in, generate a next batch
		if self._current == len(self._queries):
			self._queries = range(1,100)
			shuffle(self._queries)
			self._current = 0

		# skip to the next one (and then return the previous one)
		self._current += 1
		return self._queries[self._current-1]


	def query(self):
		'generate random TPC-H query'

		qid = self._next_id()

		template = '%d.sql' % (qid, )

		r = run_cmd(['dsqgen', '-dialect', 'postgres', '-template', template,
					 '-directory', self._templates, '-output', self._tmpdir],
					env={'PATH' : self._qgen}, cwd=self._qgen)

		return {'id' : qid, 'sql' : open('%s/query_0.sql' % (self._tmpdir,), 'r').read()}


def tpcds_worker(worker_id, dbname, result_queue, query_gen, duration=60):
	'''function executed by TPC-DS worker processes (running queries)
	
	dbname       - database to connect to (on localhost)
	result_queue - where to push results
	query_gen    - instance of QueryGenerator
	duration     - duration of the benchmark in seconds
	'''

	try:
		conn = psycopg2.connect('host=localhost dbname=%s' % (dbname,))
		conn.autocommit = True # TODO do we need autocommit?
		cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
	except:
		return

	# results for this worker process, with all counters set to 0
	results = [{'count' : 0, 'timeouts' : 0, 'failures' : 0, 'runs' : []} for i in range(99)]

	# run queries until the duration is 
	start = time.time()
	while (time.time() - start) < duration:

		q = query_gen.query()

		# TODO make this a configuration parameter
		cur.execute('SET statement_timeout = 10000')

		query_start = time.time()

		try:
			cancelled = 0
			failed = 0
			cur.execute(q['sql'])
		except QueryCanceledError:
			# TODO generate explain for cancelled queries
			cancelled = 1
		except:
			failed = 1

		# update results for this query (don't forget the queries are indexed from 0)
		# TODO perhaps we should keep more info about each execution (start/end time)
		results[q['id']-1]['count'] += 1
		results[q['id']-1]['timeouts'] += cancelled
		results[q['id']-1]['failures'] += failed

		# keep the duration only if not timed out / failed
		if cancelled == 0 and failed == 0:
			results[q['id']-1]['runs'].append(time.time() - query_start)

	result_queue.put(results)
	conn.close()
