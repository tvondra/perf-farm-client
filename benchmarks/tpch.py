import json
import math
import os
import psycopg2
import psycopg2.extras
import shutil
import time
import re

from multiprocessing import cpu_count, Process, Queue
from psycopg2.extensions import QueryCanceledError
from random import shuffle
from utils.logging import log
from utils.misc import available_ram, run_cmd


class TPCHBench(object):
	'a simple TPC-H like benchmark, measuring timings of data load and 22 queries (no data refreshes)'

	def __init__(self, bin_path, dbgen_path, cache_path, dbname, scales = [1], streams = [1]):
		'''
		bin_path   - path to PostgreSQL binaries (dropdb, createdb, psql commands)
		dbgen_path - path to the TPC-H dbgen/qgen binaries
		dbname     - name of the database to use
		scales     - list of scales to test (default: 1)
		streams    - list of stream counts to test (default: 1)
		'''

		self._bin = bin_path
		self._dbgen = dbgen_path
		self._cache = cache_path
		self._dbname = dbname
		self._scales = scales
		self._streams = streams
		self._results = {}


	def _init_database(self):
		'recreate the database and create basic schema (i.e. tables)'

		log("recreating '%s' database" % (self._dbname,))

		run_cmd(['dropdb', '--if-exists', self._dbname], env={'PATH' : self._bin})
		run_cmd(['createdb', self._dbname], env={'PATH' : self._bin})

		filename = os.path.join(os.path.dirname(__file__), 'tpch/schema.sql')
		run_cmd(['psql', self._dbname, '-c', ('\i %s' % (filename,))], env={'PATH' : self._bin})


	def _load_data(self, scale):
		'load data into the database, using existing data files (generated in advance)'

		# TODO allow loading compressed data (COPY FROM PROGRAM)

		# make sure the data directory exists
		if not os.path.exists('%s/%d' % (self._cache, scale)):
			raise Exception('data for scale %d does not exist' % (scale,))

		# load timings for each data set
		timings = {}

		for t in ['part', 'region', 'nation', 'supplier', 'customer', 'partsupp', 'orders', 'lineitem']:

			log("loading data for table '%s'" % (t,))
			sql = "COPY %(name)s FROM '%(dir)s/%(scale)d/%(name)s.csv' WITH (FORMAT csv, DELIMITER '|')" % {'dir' : self._cache, 'name' : t, 'scale' : scale}

			r = run_cmd(['psql', self._dbname, '-c', sql], env={'PATH' : self._bin})

			timings[t] = r[2]

		return timings


	@staticmethod
	def _load_script(filename):
		'load a script (with name:sql format) with filename relative to this source'

		lines = {}

		# all scripts are located in this module's subdirectory
		for l in open(os.path.join(os.path.dirname(__file__), filename), 'r'):
			tmp = [t.strip().lower() for t in l.split(':')]
			lines[tmp[0]] = tmp[1]

		return lines


	def _create_indexes(self):
		'create custom indexes on the database, return dict with timings'

		indexes = TPCHBench._load_script('tpch/indexes.sql')

		timings = {}

		for i in indexes:

			log("creating index '%s'" % (i,))

			r = run_cmd(['psql', self._dbname, '-c', indexes[i]], env={'PATH' : self._bin})
			timings[i] = r[2]

		return timings


	def _create_primary_keys(self):
		'create primary keys on the database, return dict with timings'

		keys = TPCHBench._load_script('tpch/primary-keys.sql')

		timings = {}

		for k in keys:

			log("creating primary key '%s'" % (k,))
			r = run_cmd(['psql', self._dbname, '-c', keys[k]], env={'PATH' : self._bin})
			timings[k] = r[2]

		return timings


	def _create_foreign_keys(self):
		'create foreign keys on the database, return dict with timings'

		keys = TPCHBench._load_script('tpch/foreign-keys.sql')

		timings = {}

		for k in keys:

			log("creating foreign key '%s'" % (k,))
			r = run_cmd(['psql', self._dbname, '-c', keys[k]], env={'PATH' : self._bin})
			timings[k] = r[2]

		return timings


	def _vacuum(self):
		'perform VACUUM ANALYZE on the database'

		log("vacuum analyze")
		r = run_cmd(['psql', self._dbname, '-c', 'vacuum analyze'], env={'PATH' : self._bin})
		return r[2]


	def _run_streams(self, duration, streams):
		'run a benchmark with given number of concurrent streams'

		# final results (merged from all workers)
		results = [{'count' : 0, 'timeouts' : 0, 'failures' : 0, 'runs' : []} for i in range(22)]
		result_queue = Queue()	# used to receive results from worker threads

		templatedir = os.path.join(os.path.dirname(__file__), 'tpch/templates')

		# create and start a list of workers
		workers = [Process(target=tpch_worker, args=(i+1, self._dbname, result_queue, QueryGenerator(self._dbgen, templatedir), duration)) for i in range(streams)]
		[w.start() for w in workers]

		# wait for all workers to complete
		for w in workers:
			w.join()

		# merge results from workers (simply fetch them from the queue until empty)
		while not result_queue.empty():

			r = result_queue.get()

			for i in range(22):
				results[i]['count'] += r[i]['count']
				results[i]['timeouts'] += r[i]['timeouts']
				results[i]['failures'] += r[i]['failures']
				results[i]['runs'].extend(r[i]['runs'])

		return results


	def run_tests(self, duration=60):
		'runs the TPC-H benchmark'

		results = {}

		for scale in self._scales:

			result = {}

			log('initializing database for scale %d' % (scale,))

			self._init_database()

			result['load'] = self._load_data(scale)
			result['indexes'] = self._create_indexes()
			result['primary-keys'] = self._create_primary_keys()
			result['foreign-keys'] = self._create_foreign_keys()
			result['vacuum'] = self._vacuum()

			for streams in self._streams:
				log('running benchmark with %d streams' % (streams,))
				result[str(streams)] = self._run_streams(duration, streams)

			# store results of the runs
			results[str(scale)] = result

		return results


class QueryGenerator(object):
	'''class for generating random sequence of the 22 TPC-H queries
	
	One important aspect is that it does not simply generate random sequence of
	numbers between 1 and 22, but sequence of shuffled batches of 22 queries
	(with all queries in random order). The goal is to run all queries.
	'''

	def __init__(self, qgen_path, template_path):
		'''
		qgen_path - path to the TPC-H dbgen/qgen binaries
		template_path - path to TPC-H templates (modified for PostgreSQL)
		'''

		self._qgen = qgen_path
		self._templates = template_path
		self._queries = range(1,23)
		shuffle(self._queries)
		self._current = 0


	def _next_id(self):
		'get next random query ID, either from the current batch or a new one'

		# if we're out of query IDs in, generate a next batch
		if self._current == len(self._queries):
			self._queries = range(1,23)
			shuffle(self._queries)
			self._current = 0

		# skip to the next one (and then return the previous one)
		self._current += 1
		return self._queries[self._current-1]


	def query(self):
		'generate random TPC-H query'

		qid = self._next_id()
		r = run_cmd(['qgen', str(qid)], env={'PATH' : self._qgen, 'DSS_QUERY' : self._templates}, cwd=self._qgen)

		return {'id' : qid, 'sql' : r[1]}


def tpch_worker(worker_id, dbname, result_queue, query_gen, duration=60):
	'''function executed by TPC-H worker processes (running queries)
	
	dbname       - database to connect to (on localhost)
	result_queue - where to push results
	query_gen    - instance of QueryGenerator
	duration     - duration of the benchmark in seconds
	'''

	try:
		conn = psycopg2.connect('host=localhost dbname=%s' % (dbname,))
		conn.autocommit = True # TODO do we need autocommit?
		cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
	except:
		return

	# results for this worker process, with all counters set to 0
	results = [{'count' : 0, 'timeouts' : 0, 'failures' : 0, 'runs' : []} for i in range(22)]

	# run queries until the duration is 
	start = time.time()
	while (time.time() - start) < duration:

		q = query_gen.query()

		# TODO make this a configuration parameter
		cur.execute('SET statement_timeout = 10000')

		query_start = time.time()

		try:
			cancelled = 0
			failed = 0
			cur.execute(q['sql'])
		except QueryCanceledError:
			# TODO generate explain for cancelled queries
			cancelled = 1
		except:
			failed = 1

		# update results for this query (don't forget the queries are indexed from 0)
		# TODO perhaps we should keep more info about each execution (start/end time)
		results[q['id']-1]['count'] += 1
		results[q['id']-1]['timeouts'] += cancelled
		results[q['id']-1]['failures'] += failed

		# keep the duration only if not timed out / failed
		if cancelled == 0 and failed == 0:
			results[q['id']-1]['runs'].append(time.time() - query_start)

	result_queue.put(results)
	conn.close()
